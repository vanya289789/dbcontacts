{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Logic
    ( getAllUsers, updateRow, insertRow, deleteRow
    ) where

import Database.PostgreSQL.Simple
import  Database.PostgreSQL.Simple.FromRow
import  Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.SqlQQ()

import  Database.PostgreSQL.Simple.ToField

import Database.PostgreSQL.Simple.Types ()
import Consts

import PgNamed
import GHC.Int()
import Numeric()

import Control.Monad.IO.Class()
import Control.Monad.Error.Class()
import Control.Monad()
import Control.Monad.Except ( runExceptT, ExceptT )

import Control.Exception()
import Data.Text ()

import Database.PostgreSQL.Query.TH()
import GHC.Base()
import ParamsChecking

data TestField = TestField {contactID::Int, phone::Int, email::String, name::String} deriving (Show)
instance FromRow TestField where
  fromRow = TestField <$> field <*> field <*> field <*> field

instance ToRow TestField where
    toRow r = [toField (contactID r), toField (phone r), toField (email r), toField (name r)]

showTestField::TestField->String 
showTestField (TestField idCustom phoneCustom emailCustom nameCustom) = "id = " ++ (show idCustom) 
    ++ "; phone = " ++ (show phoneCustom) ++ "; email = " ++ emailCustom
        ++ "; name = " ++ nameCustom

showNew:: [TestField] -> Int -> String
showNew (x:xs) number = "\n" ++ (show number) ++ ". " ++ (showTestField x) ++ (showNew xs (number + 1))
showNew _ _ = ""


getAllUsers::[String] -> IO()
getAllUsers inputParams = do
    let isCorrectInput = canGenInputParams inputParams
    if not isCorrectInput then
        putStrLn (unknownMsg ++ "\n" ++ helpMsg)
    else do
        c <- connect defaultConnectInfo { connectPort = 8080 }
        let paramsKeyVal = getInputParams inputParams
        let nonParams = getMissedParams paramsKeyVal allParamsNames
        let selectString = selectExec paramsKeyVal nonParams
        let selectParamsCustom = getSelectParams paramsKeyVal
        if (null selectParamsCustom) then do
            s <- query_ c "SELECT * FROM public.\"contactsID\" WHERE 1 = 1" :: IO[TestField]
            putStrLn (showNew s 0)
            return()
        else do
            s <- runDB [] $ queryNamed c selectString selectParamsCustom
            putStrLn (showNew s 0)
            return()
        close c

insertRow::[String]->IO()
insertRow inputParams = do
    let isCorrectInput = canGenInputParams inputParams
    if not isCorrectInput then
        putStrLn (unknownMsg ++ "\n" ++ helpMsg)
    else do
        c <- connect defaultConnectInfo { connectPort = 8080 }
        let paramsKeyVal = getInputParams inputParams
        let insertParams = getInsertParams paramsKeyVal
        s <- runDB (-1) $ executeNamed c insertBegin insertParams
        if s /= 1 then
            putStrLn badExec 
        else
            putStrLn successExec 
        putStrLn $ show s
        close c

updateRow::[String] -> IO()
updateRow (idCustom:inputParams) = do
    let isCorrectInput = (isVarNumber idCustom) && (canGenInputParams inputParams)
    if not isCorrectInput then
        putStrLn (unknownMsg ++ "\n" ++ helpMsg)
    else do
        c <- connect defaultConnectInfo { connectPort = 8080 }
        let paramsKeyVal = getInputParams inputParams
        let intID = (read idCustom) :: Int
        let updateString = updateExec paramsKeyVal
        let updateParamsCustom = getUpdateParams paramsKeyVal intID
        s <- runDB (-1) $ executeNamed c updateString updateParamsCustom
        if s /= 1 then
            putStrLn badExec 
        else
            putStrLn successExec 
        close c

deleteRow::[String] -> IO()
deleteRow (idCustom:_) = do
    let isCorrectInput = (isVarNumber idCustom)
    if not isCorrectInput then
        putStrLn (unknownMsg ++ "\n" ++ helpMsg)
    else do
        c <- connect defaultConnectInfo { connectPort = 8080 }
        let intID = (read idCustom) :: Int
        s <- runDB (-1) $ executeNamed c deleteBegin ["id" =? intID]
        if s /= 1 then
            putStrLn badExec 
        else
            putStrLn successExec 
        close c

selectParams::[(String, String)]->Query
selectParams ((x, _):xs) = case (x) of
    "id" -> mconcat( ["AND \"ID\" = ?id "] ++ [(selectParams xs)] )
    "phone" -> mconcat( ["AND \"phone\" = ?phone "] ++ [(selectParams xs)] )
    "email" -> mconcat( ["AND \"email\" LIKE ?email "] ++ [(selectParams xs)] )
    "name" -> mconcat( ["AND \"name\" LIKE ?name "] ++ [(selectParams xs)] )
    _ -> mconcat( [""] ++ [(selectParams xs)] )
selectParams _ = ""

updateParams::[(String, String)]->Query
updateParams ((x, _):xs) = case (x) of
    "phone" -> mconcat( [",\"phone\" = ?phone "] ++ [(updateParams xs)] )
    "email" -> mconcat( [",\"email\" = ?email "] ++ [(updateParams xs)] )
    "name" -> mconcat( [",\"name\" = ?name "] ++ [(updateParams xs)] )
    _ -> mconcat( [""] ++ [(updateParams xs)] )
updateParams _ = ""

updateExec::[(String, String)]->Query
updateExec list = mconcat([updateBegin] ++ [(updateParams list)] ++ [updateEnd])

selectExec::[(String, String)]->[String]->Query
selectExec list _ = mconcat([selectBegin] ++ [(selectParams list)])

getNamedParamForKey::[(String, String)]->String->Name->String->Bool->[NamedParam]
getNamedParamForKey source val val2 typeElem isSelect = do
    let findVal = findNeededValue source val
    if findVal then do
        let valElem = getNeededValue source val
        if typeElem == "Int" then
            [val2 =? ((read valElem)::Int)]
        else do
            if isSelect then
                [val2 =? "%" ++ valElem ++ "%"]
            else 
                [val2 =? valElem]
    else []

getSelectParams::[(String, String)]->[NamedParam]
getSelectParams source = do
    (getNamedParamForKey source "id" "id" "Int" True)
        ++ (getNamedParamForKey source "phone" "phone" "Int" True)
            ++ (getNamedParamForKey source "email" "email" "Varchar" True)
                ++ (getNamedParamForKey source "name" "name" "Varchar" True)

getInsertParams::[(String, String)]->[NamedParam]
getInsertParams source = do
    let findPhone = findNeededValue source "phone"
    let findEmail = findNeededValue source "email"
    let findName = findNeededValue source "name"
    ["phone" =? if findPhone then read (getNeededValue source "phone") ::Int else -1,
        "email" =? if findEmail then getNeededValue source "email" ::String else "",
        "name" =? if findName then getNeededValue source "name" ::String else ""]

getUpdateParams::[(String, String)]->Int->[NamedParam]
getUpdateParams source idTmp = do
    ["id" =? idTmp]
        ++ (getNamedParamForKey source "phone" "phone" "Int" False)
            ++ (getNamedParamForKey source "email" "email" "Varchar" False)
                ++ (getNamedParamForKey source "name" "name" "Varchar" False)

runDB :: a -> ExceptT PgNamedError IO a -> IO a
runDB defaultVal dbResult = do
   eitherResult <- runExceptT dbResult
   case eitherResult of
    Left _ -> do
        return defaultVal
    Right res -> return res
