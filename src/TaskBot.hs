{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module TaskBot
  ( runBot
  ) where

import Consts
import System.IO

import Logic

import Data.List.Split

runBot :: IO ()
runBot = do
  -- disable buffering for stdout
  hSetBuffering stdout NoBuffering
  putStrLn namePrompt
  go
  where
    -- Helper function to interact with user and update tasks list
    go :: IO ()
    go = do
      inputStr <- getLine
      let strList = splitOn spliter inputStr
      let inputCmd = head strList
      let params = tail strList
      if (inputCmd == "/exit")
        then putStrLn goodbyeMsg
        else do
          processCommand inputCmd params
          go

processCommand::String -> [String]-> IO()
processCommand cmd paramsArgs = case cmd of
  "/list" -> do
    getAllUsers paramsArgs
  "/update" -> do
    updateRow paramsArgs
  "/insert" -> do
    insertRow paramsArgs
  "/delete" -> do
    deleteRow paramsArgs
  "/help" -> do
    putStrLn helpMsg
  _ -> do
    putStrLn unknownMsg
