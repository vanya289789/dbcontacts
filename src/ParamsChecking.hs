{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module ParamsChecking
    ( isVarNumber, canGenInputParams, getInputParams, findNeededValue, getNeededValue, getMissedParams
    ) where

import Data.List.Split

import Text.Read

import Consts

isVarNumber::String->Bool 
isVarNumber strInt = case(readMaybe strInt :: Maybe Int) of
    Nothing -> False 
    _ -> True

canGenInputParams::[String]->Bool 
canGenInputParams argsList = all (\s -> let keyVal = splitOn spliterKeyVal s in 
    (not (null $ tail keyVal)) ) argsList

getInputParams::[String]->[(String, String)]
getInputParams argsList = map
    (\s -> let keyVal = splitOn spliterKeyVal s in (head keyVal, head (tail keyVal))) argsList

findNeededValue::[(String, String)]->String->Bool
findNeededValue ((x, _):xs) val = (x == val) || (findNeededValue xs val)
findNeededValue _ _ = False 

getNeededValue::[(String, String)]->String->String
getNeededValue ((x, y):xs) val = if (x == val) then y else (getNeededValue xs val)
getNeededValue _ _ = ""

getMissedParams::[(String,String)]->[String]->[String]
getMissedParams list (x:xs) = if findNeededValue list x then getMissedParams list xs
    else (x : (getMissedParams list xs))
getMissedParams _ _ = []