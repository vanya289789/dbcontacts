{-# LANGUAGE OverloadedStrings #-}
module Consts where
import Database.PostgreSQL.Query (Query)

-- | Messages used for bot interactio.
namePrompt, goodbyeMsg, unknownMsg, helpMsg, spliter, spliterKeyVal, successExec, badExec :: String
namePrompt = "Enter your command. To see listing enter /help: "
goodbyeMsg = "Goodbye!"
unknownMsg = "I do not unsertand you!"
helpMsg = "There are some commands:\n" ++ "1. /insert params\n" ++ "2. /update id params\n" ++ "3. /list params\n"
    ++ "4. /delete id\n" ++ "5. /help\n" ++ "6. /exit\n" ++ "\n" ++ "params must me written in format: *key*=*value*..." 
        ++ "\nkey may be:id (Int), phone (Int), email (Varchar),name (Varchar)" 
            ++ "\nExample: /update 5 phone=10 email=tmp name=tmp"
spliter = " "
spliterKeyVal = "="
successExec = "Operation was executed succesfully"
badExec = "Some errors were during execution"

selectBegin, updateBegin, updateEnd, insertBegin, deleteBegin :: Query
selectBegin = "SELECT * FROM public.\"contactsID\" WHERE 1 = 1 "
updateBegin = "UPDATE public.\"contactsID\" SET \"ID\" = \"ID\" "
updateEnd = " WHERE \"ID\" = ?id"
insertBegin = "INSERT INTO public.\"contactsID\" (\"phone\", \"email\", \"name\") VALUES(?phone, ?email, ?name)"
deleteBegin = "DELETE FROM public.\"contactsID\" WHERE \"ID\" = ?id"
--RETURNING \"ID\";
-- имена колонок как в БД
allParamsNames::[String]
allParamsNames = ["id", "phone", "email", "name"]
