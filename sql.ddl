CREATE SEQUENCE public.contactsidseq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE public."contactsID" (
	"ID" int4 NOT NULL DEFAULT nextval('public.contactsidseq'::text::regclass),
	phone int4 NOT NULL,
	email varchar NULL,
	"name" varchar NULL,
	CONSTRAINT "PK_Contacts" PRIMARY KEY ("ID")
);

